﻿using System;
using UnityEngine;

public class BasicMovementController : MonoBehaviour
{
    public event Action<float> OnMoved;
    
    [SerializeField] private float _speed;
    [SerializeField] private float _angularSpeed;

    private Vector3? _targetPosition;

    private void FixedUpdate()
    {
        // Некоторые сравнение float значений хотел делать с помощью Mathf.Approximately, но точность некоторый
        // методов из той же библиотеки осавляет желать лучшего, поэтому закастылено "магическими" числами 
        if (_targetPosition.HasValue)
        {
            var difference = transform.position - _targetPosition.Value;
            var xzMagnitude = difference.x * difference.x + difference.z * difference.z;
            var needToMove = xzMagnitude > 0.00001f; // кастыль

            if (needToMove)
            {
                var localTarget = transform.InverseTransformPoint(_targetPosition.Value);
                var angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

                if (Mathf.Abs(angle) > 0.0001f) // кастыль
                {
                    var speed = _angularSpeed * Time.fixedDeltaTime;
                    var rotateAngle = Mathf.Clamp(angle, -speed, speed);

                    transform.Rotate(Vector3.up, rotateAngle);
                }
                else
                {
                    var speed = _speed * Time.fixedDeltaTime;
                    var distance = Mathf.Clamp(speed, 0f, Mathf.Sqrt(xzMagnitude));

                    transform.position += (transform.forward * distance);
                    OnMoved?.Invoke(distance);
                }
            }
            else
                _targetPosition = null;
        }
    }
    
    public void SetDestination(Vector3 targetPosition)
    {
        _targetPosition = targetPosition;
    }
}
