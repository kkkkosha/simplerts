﻿using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Camera))]
public class CameraMovement : MonoBehaviour
{
    [SerializeField] private float _sensetivity;
    
    private Camera _camera;
    private Vector2 _mousePositionInScreenSpace;
    
    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (Mouse.current.middleButton.wasPressedThisFrame)
            _mousePositionInScreenSpace = Mouse.current.position.ReadValue();

        if (Mouse.current.middleButton.isPressed)
        {
            var newMousePosition = Mouse.current.position.ReadValue();
            var delta = (Vector3) (_mousePositionInScreenSpace - newMousePosition);

            delta.z = delta.y;
            delta.y = 0;
            delta = Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up) * delta;

            transform.position += delta * (_sensetivity * Time.deltaTime);
            _mousePositionInScreenSpace = newMousePosition;
        }
    }
}
