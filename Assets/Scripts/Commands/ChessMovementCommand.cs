﻿using System.Collections.Generic;
using UnityEngine;

public class ChessMovementCommand : IMovementCommand
{
    private const float CellLength = 4f;
    
    private Dictionary<IMovable, Vector3> _offsets = new Dictionary<IMovable, Vector3>();

    public string MovementTypeName => "Chess";

    public void CalculateDestinations(List<IMovable> movables)
    {
        _offsets.Clear();

        if (movables.Count == 0)
            return;
        
        var quadSide = (int) Mathf.Ceil(Mathf.Sqrt(movables.Count));
        var rows = (int) Mathf.Ceil(movables.Count / (float) quadSide);
        var movableIndex = 0;
        var startX = -((quadSide - 1) * CellLength) / 2;
        var startZ = ((rows - 1) * CellLength) / 2;

        for (var rowIndex = 0; rowIndex < rows; rowIndex++)
        {
            var x = (rowIndex & 1) == 0 ? startX : startX + CellLength / 2f;
            var z = startZ - CellLength * rowIndex;

            for (var columnIndex = 0; columnIndex < quadSide && movableIndex < movables.Count; columnIndex++)
            {
                var movable = movables[movableIndex++];
                var offset = new Vector3(
                    x + CellLength * columnIndex, 
                    0f, 
                    z
                );

                _offsets[movable] = offset;
            }
        }
    }

    public void MoveAll(Vector3 target)
    {
        foreach (var pair in _offsets)
        {
            var movable = pair.Key;
            
            movable.MoveTo(pair.Value + target);
        }
    }
}

