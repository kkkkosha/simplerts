﻿using System.Collections.Generic;
using UnityEngine;

public class CircleMovementCommand : IMovementCommand
{
    private Dictionary<IMovable, Vector3> _offsets = new Dictionary<IMovable, Vector3>();
    private float _radius = 5f;

    public string MovementTypeName => "Circle";
    
    public float Radius
    {
        get => _radius;
        set => _radius = value < 0f ? 0f : value;
    }

    public CircleMovementCommand(float radius = 5f)
    {
        Radius = radius;
    }
    
    public void CalculateDestinations(List<IMovable> movables)
    {
        _offsets.Clear();
        
        if (movables.Count == 0)
            return;
        
        if (movables.Count == 1)
        {
            _offsets[movables[0]] = Vector2.zero;
            return;
        }

        var startAngle = (movables.Count & 1) == 0 ? 0f : 90f;
        var deltaAngle = 360f / (movables.Count);

        for (var i = 0; i < movables.Count; i++)
        {
            var angle = (startAngle + i * deltaAngle) * Mathf.Deg2Rad;
            var x = Mathf.Cos(angle);
            var z = Mathf.Sin(angle);
            
            _offsets.Add(movables[i],  new Vector3(x, 0f, z));
        }
    }

    public void MoveAll(Vector3 target)
    {
        foreach (var pair in _offsets)
        {
            var movable = pair.Key;
            
            movable.MoveTo(pair.Value * _radius + target);
        }
    }
}