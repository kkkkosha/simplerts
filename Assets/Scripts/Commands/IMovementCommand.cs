﻿using System.Collections.Generic;
using UnityEngine;

public interface IMovementCommand
{
    string MovementTypeName { get; }
    void CalculateDestinations(List<IMovable> movables);
    void MoveAll(Vector3 target);
}