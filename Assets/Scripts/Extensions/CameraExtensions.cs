﻿using UnityEngine;

public static class CameraExtensions
{
    public static bool RaycastScreenPoint(this Camera camera, Vector3 screenPoint, int layerMask, out Vector3 hitPoint)
    {
        var ray = camera.ScreenPointToRay(screenPoint);
        
        if (Physics.Raycast(ray, out var hitInfo, camera.farClipPlane, layerMask))
        {
            hitPoint = hitInfo.point;

            return true;
        }

        hitPoint = camera.transform.position;
        
        return false;
    }
}
