﻿using UnityEngine;

public interface ISelectingAreaDrawer
{
    void StartDrawing();
    void UpdateArea(Rect rect);
    void StopDrawing();
}
