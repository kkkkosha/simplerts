﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageSelectionDrawer : MonoBehaviour, ISelectingAreaDrawer
{
    private RectTransform _transform;

    private void Start()
    {
        _transform = transform as RectTransform;
        gameObject.SetActive(false);
    }

    public void StartDrawing()
    {
        _transform.anchoredPosition = Vector2.zero;
        _transform.sizeDelta = Vector2.zero;
        gameObject.SetActive(true);
    }

    public void UpdateArea(Rect rect)
    {
        _transform.anchoredPosition = new Vector2(rect.x, rect.y);
        _transform.sizeDelta = new Vector2(rect.width, rect.height);
    }

    public void StopDrawing()
    {
        gameObject.SetActive(false);
    }
}