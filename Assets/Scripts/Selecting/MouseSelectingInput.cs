﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class MouseSelectingInput : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    public event Action SelectingStarted;
    public event Action<Rect> Selecting;
    public event Action<Rect> Selected;
    
    private Vector2 _startPoint;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Mouse.current.rightButton.wasPressedThisFrame || Mouse.current.middleButton.wasPressedThisFrame)
            return;
        
        StartArea(eventData.position);
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        if (!Mouse.current.leftButton.isPressed)
            return;
        
        UpdateArea(eventData.position);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!Mouse.current.leftButton.wasReleasedThisFrame)
            return;
        
        EndArea(eventData.position);
    }

    private void StartArea(Vector2 screenPoint)
    {
        _startPoint = screenPoint;
        SelectingStarted?.Invoke();
    }
    
    private void UpdateArea(Vector2 screenPoint)
    {
        Selecting?.Invoke(GetSelectedRect(_startPoint, screenPoint));
    }
    
    private void EndArea(Vector2 screenPoint)
    {
        Selected?.Invoke(GetSelectedRect(_startPoint, screenPoint));
    }

    private static Rect GetSelectedRect(Vector2 startPoint, Vector2 endPoint)
    {
        if (startPoint.x > endPoint.x)
        {
            var tmpX = startPoint.x;
            startPoint.x = endPoint.x;
            endPoint.x = tmpX;
        }
        
        if (startPoint.y > endPoint.y)
        {
            var tmpY = startPoint.y;
            startPoint.y = endPoint.y;
            endPoint.y = tmpY;
        }
        
        return new Rect()
        {
            x = startPoint.x,
            y = startPoint.y,
            width = endPoint.x - startPoint.x,
            height = endPoint.y - startPoint.y
        };
    }
}
