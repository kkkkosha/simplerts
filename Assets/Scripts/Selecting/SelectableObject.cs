﻿using UnityEngine;

public abstract class SelectableObject : MonoBehaviour, ISelectable
{
    public abstract void OnSelect();

    public abstract void OnDeselect();
}
