﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SelectedUnitsController : MonoBehaviour
{
    public readonly List<IMovementCommand> MovementCommands = new List<IMovementCommand>()
    {
        new CircleMovementCommand(),
        new ChessMovementCommand()
    };
    
    [SerializeField] private UserSelect _unitProvider;
    [SerializeField] private Camera _camera;
    [SerializeField] private LayerMask _selectableLayerMask;

    private IMovementCommand _currentMovementCommand;
    
    public bool FollowCursor { get; set; }

    public IMovementCommand CurrentMovementCommand
    {
        get => _currentMovementCommand;
        set
        {
            _currentMovementCommand = value;
            _currentMovementCommand.CalculateDestinations(
                new List<IMovable>(_unitProvider.GetSelectedObjects<IMovable>()));
        }
    }

    private void Awake()
    {
        if (MovementCommands.Count > 0)
            CurrentMovementCommand = MovementCommands[0];
    }

    private void OnEnable()
    {
        _unitProvider.SelectedChanged += OnSelectedChanged;
    }

    private void OnDisable()
    {
        _unitProvider.SelectedChanged -= OnSelectedChanged;
    }

    private void Update()
    {
        if (Keyboard.current.deleteKey.wasReleasedThisFrame)
            DeleteSelectedUnits();
        
        if ((Mouse.current.rightButton.wasPressedThisFrame || FollowCursor) &&
             _camera.RaycastScreenPoint(Mouse.current.position.ReadValue(), ~_selectableLayerMask, out var hitPoint))
            CurrentMovementCommand.MoveAll(hitPoint);
    }
    
    private void DeleteSelectedUnits()
    {
        foreach (var unit in _unitProvider.Selected)
        {
            unit.OnDeselect();
            
            if (unit is IRemovable removable)
                removable.Remove();
        }
        
        _unitProvider.ClearSelection();
    }
    
    private void OnSelectedChanged()
    {
        CurrentMovementCommand.CalculateDestinations(new List<IMovable>(_unitProvider.GetSelectedObjects<IMovable>()));
    }
}
