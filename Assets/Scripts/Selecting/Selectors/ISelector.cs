﻿using System.Collections.Generic;
using UnityEngine;

public interface ISelector
{
    IEnumerable<ISelectable> SelectInScreenSpace(Rect rect);
    ISelectable SelectInScreenSpace(Vector3 point);
}
