﻿using System.Collections.Generic;
using UnityEngine;

public class ScreenProjectionSelector : MonoBehaviour, ISelector
{
    [SerializeField] private Camera _camera;
    
    public IEnumerable<ISelectable> SelectInScreenSpace(Rect rect)
    {
        foreach (var selectableObject in FindObjectsOfType<SelectableObject>())
        {
            var screenPoint = _camera.WorldToScreenPoint(selectableObject.transform.position);

            if (rect.Contains(screenPoint))
                yield return selectableObject;
        }
    }

    public ISelectable SelectInScreenSpace(Vector3 point)
    {
        var ray = _camera.ScreenPointToRay(point);
        var raycastHits = Physics.RaycastAll(ray, _camera.farClipPlane);

        foreach (var hit in raycastHits)
        {
            var selectableObject = hit.collider.GetComponent<ISelectable>();

            if (selectableObject != null)
                return selectableObject;
        }

        return null;
    }
}