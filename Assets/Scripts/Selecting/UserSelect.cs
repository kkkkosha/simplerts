﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UserSelect : MonoBehaviour
{
    public event Action SelectedChanged;
    
    [SerializeField] private MouseSelectingInput _input;
    
    private ISelector _selector;
    private ISelectingAreaDrawer _drawer;

    public HashSet<ISelectable> Selected { get; private set; } = new HashSet<ISelectable>();
    
    private void Awake()
    {
        _selector = GetComponentInChildren<ISelector>();
        _drawer = GetComponentInChildren<ISelectingAreaDrawer>();
    }

    private void OnEnable()
    {
        _input.SelectingStarted += OnSelectingStarted;
        _input.Selecting += OnSelecting;
        _input.Selected += OnSelected;
    }

    private void OnDisable()
    {
        _input.SelectingStarted -= OnSelectingStarted;
        _input.Selecting -= OnSelecting;
        _input.Selected -= OnSelected;
    }

    private void OnSelectingStarted()
    {
        _drawer.StartDrawing();
    }

    private void OnSelecting(Rect rect)
    {
        _drawer.UpdateArea(rect);
    }

    private void OnSelected(Rect rect)
    {
        _drawer.StopDrawing();

        var isSelectedChanged = false;
        var selected = new HashSet<ISelectable>(_selector.SelectInScreenSpace(rect));
        var selectedInCenter = _selector.SelectInScreenSpace(rect.center);

        if (selectedInCenter != null && !selected.Contains(selectedInCenter))
            selected.Add(selectedInCenter);
        
        foreach (var selectable in selected)
        {
            if (!Selected.Contains(selectable))
            {
                selectable.OnSelect();
                isSelectedChanged = true;
            }
        }

        foreach (var selectable in Selected)
        {
            if (!selected.Contains(selectable))
                selectable.OnDeselect();
            
            isSelectedChanged = true;
        }

        Selected = selected;
        
        if (isSelectedChanged)
            SelectedChanged?.Invoke();
    }

    public IEnumerable<T> GetSelectedObjects<T>()
    {
        foreach (var selectable in Selected)
        {
            if (selectable is T convertedSelectable)
                yield return convertedSelectable;
        }
    }

    public void ClearSelection(bool deselect = true)
    {
        if (deselect)
        {
            foreach (var selectable in Selected)
                selectable.OnDeselect();
        }
        
        Selected.Clear();
        SelectedChanged?.Invoke();
    }
}
