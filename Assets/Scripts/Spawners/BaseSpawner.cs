﻿using UnityEngine;

public class BaseSpawner<T> : MonoBehaviour where T : MonoBehaviour
{
    [SerializeField] protected T Prefab;
    
    public T SpawnObject(Vector3 position)
    {
        return Instantiate(Prefab, position, Quaternion.identity);
    }
}