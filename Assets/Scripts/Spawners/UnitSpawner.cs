﻿using UnityEngine;
using UnityEngine.InputSystem;

public class UnitSpawner : BaseSpawner<Unit>
{
    private void Update()
    {
        if (Keyboard.current.sKey.wasReleasedThisFrame)
            SpawnObject(Vector3.zero);
    }
    
    
}