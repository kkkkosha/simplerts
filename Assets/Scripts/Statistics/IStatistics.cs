﻿using System;

public interface IStatistics
{
    event Action Changed;
}