﻿ using System;

 public class UnitStatistics : IStatistics
 {
     public event Action Changed;
     
     private float _passedDistanceUnits;
     private int _selectedCount;

     public float PassedDistanceUnits => _passedDistanceUnits;
     public int SelectedCount => _selectedCount;

     public void AddPassedDistanceUnits(float distance)
     {
         if (distance <= 0f)
             return;

         var newPassedDistance = _passedDistanceUnits + distance;
         
         if ((int) _passedDistanceUnits < (int) newPassedDistance)
         {
             _passedDistanceUnits = newPassedDistance;
             Changed?.Invoke();
         }
         else
             _passedDistanceUnits = newPassedDistance;
     }

     public void OnUnitSelected()
     {
         _selectedCount++;
         Changed?.Invoke();
     }

     public override string ToString()
     {
         return $"Selected: {SelectedCount} times\nDistance passed: {PassedDistanceUnits:0}\n";
     }
 }
