﻿using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class CircleMovementSettingsView : MonoBehaviour
{
    [SerializeField] private SelectedUnitsController _selectedUnitsController;
    [SerializeField] private TMP_InputField _radiusInputField;

    private CircleMovementCommand _movementCommand;
    
    private float Radius
    {
        get => float.TryParse(_radiusInputField.text, out var result) ? result : 0f;
        set => _radiusInputField.text = value.ToString();
    }
    
    private void Awake()
    {
        foreach (var movementCommand in _selectedUnitsController.MovementCommands)
        {
            if (movementCommand is CircleMovementCommand circleMovementCommand)
            {
                _movementCommand = circleMovementCommand;
                break;
            }
        }
        
        _radiusInputField.contentType = TMP_InputField.ContentType.DecimalNumber;
        _radiusInputField.onSelect.AddListener(OnInputFieldSelect);
        _radiusInputField.onDeselect.AddListener(OnInputFieldDeselect);
        _radiusInputField.onValueChanged.AddListener(OnRadiusValueChanged);

        if (_movementCommand != null)
            Radius = _movementCommand.Radius;
    }

    private void OnRadiusValueChanged(string value)
    {
        if (_movementCommand != null)
            _movementCommand.Radius = Radius;
    }

    private void OnInputFieldDeselect(string arg)
    {
        InputSystem.EnableDevice(Keyboard.current);
    }

    private void OnInputFieldSelect(string arg)
    {
        InputSystem.DisableDevice(Keyboard.current);
    }
}