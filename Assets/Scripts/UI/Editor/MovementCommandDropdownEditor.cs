﻿using UnityEditor;
using TMPro.EditorUtilities;

[CustomEditor(typeof(MovementCommandDropdown))]
public class MovementCommandDropdownEditor : DropdownEditor
{
    private SerializedProperty _selectedUnitsController;
    
    protected override void OnEnable()
    {
        base.OnEnable();
        _selectedUnitsController = serializedObject.FindProperty("_selectedUnitsController");
    }
    
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(_selectedUnitsController);
        serializedObject.ApplyModifiedProperties();

        base.OnInspectorGUI();
    }
}