﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MovementCommandDropdown : TMP_Dropdown
{
    [SerializeField] private SelectedUnitsController _selectedUnitsController;
    
    public List<IMovementCommand> MovementCommands => _selectedUnitsController.MovementCommands;
    
    protected override void Awake()
    {
        base.Awake();
        var optionsList = new List<OptionData>(MovementCommands.Count);

        foreach (var movementCommand in MovementCommands)
            optionsList.Add(new OptionData(movementCommand.MovementTypeName));
        
        ClearOptions();
        AddOptions(optionsList);
        RefreshShownValue();
        
        onValueChanged.AddListener(OnValueChanged);
    }

    private void OnValueChanged(int index)
    {
        _selectedUnitsController.CurrentMovementCommand = MovementCommands[index];
    }
}