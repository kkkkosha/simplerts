﻿using UnityEngine;
using UnityEngine.UI;

public class MovementSettingsView : MonoBehaviour
{
    [SerializeField] private SelectedUnitsController _selectedUnitsController;
    [SerializeField] private Toggle _followMouseToggle;
    
    private void Awake()
    {
        _followMouseToggle.isOn = _selectedUnitsController.FollowCursor;
        _followMouseToggle.onValueChanged.AddListener(OnFollowMouseChanged);
    }

    private void OnFollowMouseChanged(bool value)
    {
        _selectedUnitsController.FollowCursor = value;
    }
}
