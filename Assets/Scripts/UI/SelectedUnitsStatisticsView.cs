﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedUnitsStatisticsView : MonoBehaviour
{
    private const int ViewsCountLimit = 100;

    [SerializeField] private UserSelect _unitProvider;
    [SerializeField] private ScrollRect _scrollRect;
    [SerializeField] private UnitStatisticsView _template;

    private List<UnitStatisticsView> _unitStatisticsViews = new List<UnitStatisticsView>();

    private void OnEnable()
    {
        _unitProvider.SelectedChanged += OnSelectedChanged;
    }

    private void OnDisable()
    {
        _unitProvider.SelectedChanged -= OnSelectedChanged;
    }

    private void OnSelectedChanged()
    {
        var units = new List<Unit>(_unitProvider.GetSelectedObjects<Unit>());
        var additionalViewsCount =
            (units.Count > ViewsCountLimit ? ViewsCountLimit : units.Count) - _unitStatisticsViews.Count;

        for (var i = 0; i < additionalViewsCount; i++)
        {
            var view = Instantiate(_template, _scrollRect.content);
            _unitStatisticsViews.Add(view);
        }

        for (var i = 0; i < -additionalViewsCount; i++)
        {
            var index = _unitStatisticsViews.Count - 1;
            var view = _unitStatisticsViews[index];
            
            _unitStatisticsViews.RemoveAt(index);
            view.Unit = null;
            Destroy(view.gameObject);
        }

        for (var i = 0; i < _unitStatisticsViews.Count; i++)
            _unitStatisticsViews[i].Unit = units[i];

        _scrollRect.verticalNormalizedPosition = 1f;
    }
}