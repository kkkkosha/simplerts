﻿using UnityEngine;
using TMPro;

public class UnitStatisticsView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textField;
    
    private Unit _unit;

    public Unit Unit
    {
        get => _unit;
        set
        {
            if (_unit != null)
                _unit.Statistics.Changed -= OnStatisticsChanged;

            _unit = value;
            
            if (enabled && _unit != null)
                _unit.Statistics.Changed += OnStatisticsChanged;
            
            Refresh();
        }
    }

    private void OnEnable()
    {
        if (_unit != null)
            _unit.Statistics.Changed += OnStatisticsChanged;
        
        Refresh();
    }

    private void OnDisable()
    {
        if (_unit != null)
            _unit.Statistics.Changed -= OnStatisticsChanged;
    }

    private void OnStatisticsChanged()
    {
        Refresh();
    }

    public void Refresh()
    {
        _textField.text = _unit != null ? _unit.Statistics.ToString() : "";
    }
}