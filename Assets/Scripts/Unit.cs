﻿using UnityEngine;

[RequireComponent(typeof(BasicMovementController))]
public class Unit : SelectableObject, IRemovable, IMovable
{
    [SerializeField] private GameObject _selectionView;

    private UnitStatistics _statistics;
    private BasicMovementController _movement;

    public UnitStatistics Statistics => _statistics;

    private void Awake()
    {
        _statistics = new UnitStatistics();
        _movement = GetComponent<BasicMovementController>();
        OnDeselect();
    }

    private void OnEnable()
    {
        _movement.OnMoved += OnMoved;
    }

    private void OnDisable()
    {
        _movement.OnMoved -= OnMoved;
    }

    private void OnMoved(float distance)
    {
        _statistics.AddPassedDistanceUnits(distance);
    }

    public override void OnSelect()
    {
        _selectionView.SetActive(true);
        _statistics.OnUnitSelected();
    }

    public override void OnDeselect()
    {
        _selectionView.SetActive(false);
    }

    public void Remove()
    {
        Destroy(gameObject);
    }

    public void MoveTo(Vector3 target)
    {
        _movement.SetDestination(target);
    }
}
